//www.dookay.com技术支持
;(function(){
	$.fn.extend({
		"lastObj":function(options){
				options=$.extend({
						tagblock:"li"//最后的标签
					},options);
			this.find(options.tagblock+':last').addClass("ts_last");	
			return this;
		}
	});
})(jQuery);//last
;(function(){
	$.fn.extend({
		"scrollexpansion":function(options){
				options=$.extend({
						tag:'a',
						n:6
					},options);					
			this.children().each(function(){
				var $Spta=$(this).find(options.tag);
				var nCount=Math.ceil($Spta.length/options.n);	
				for(var i=0; i< nCount;i++){
					var newSpt=$Spta.slice(options.n*i,options.n*i+options.n);
					var $div=$("<div></div>")
					$div.append(newSpt)
					$(this).append($div)
				}		
			});			
			return this;
		}
	});
})(jQuery);//扩展scrollable的插件
;(function(){
	$.fn.extend({
		"showHide":function(options){
				options=$.extend({
						tagblock:"dl",//显示
						delay:300,//延迟
						speed:200//显示速度
					},options);
			this.hover(function(){
				$(this).css({'position':'relative','z-index':'1'}).addClass('shw_cur').children(':first').addClass('chd_cur').siblings(options.tagblock).css({'position':'absolute','z-index':'1'}).delay(options.delay).slideDown(options.speed);
			},function(){
				$(this).removeClass('shw_cur').removeAttr('style').children(':first').removeClass('chd_cur').siblings(options.tagblock).removeAttr('style').stop(true,false).hide();	
			});	
			return this;
		}
	});
})(jQuery);//显示隐藏
//引用-----------------------------------------------------------------------------
$(function(){
	$(".table table table").hover(function(){
			$(this).addClass("show_body");
		},function(){
			$(this).removeClass("show_body");
		}
	)
	$(".table_email td").hover(function(){
			$(this).addClass("td_hover");
		},function(){
			$(this).removeClass("td_hover");
		}
	)
	$(".table1 td").hover(function(){
			$(this).addClass("td_hover");
		},function(){
			$(this).removeClass("td_hover");
		}
	)
	$('.last').lastObj();//最后一个li加类
	$("input[rel=#send_email]").overlay({ mask: '#000' });
	$("a[rel=#xunjia]").overlay({ mask: '#000' });
	$("a[rel=#bespeak]").overlay({ mask: '#000' });
	$('.table_gz tr:last').addClass('tr_last');
	$('.table_zntj tr:last').addClass('tr_last');
	$("div.tabs_ajax").tabs("div.tabcnt_ajax", { effect: 'ajax' }); //ajax tab
	$('.xialabeizhu').toggle(
		function(){
			$('.xialabeizhu p').removeClass();
			$('.xialabeizhu span').addClass("arrow");
		},
		function(){
			$('.xialabeizhu p').addClass("beizhu");
			$('.xialabeizhu span').removeClass();
		}
	)
	$("ul.tabs").tabs("div.panes > div");
	$('.btn_fb').click(function(){
			$('.feedback').hide();
			$('.form_fd').show();
			return false;
		}
	)
	$('.can_fd').click(function(){
			$('.form_fd').hide();
			$('.feedback').show();
			return false;
		}
	)
});