$(function(){
	
var map = new BMap.Map("map");// 创建Map实例
var point=map.centerAndZoom(new BMap.Point(121.502939,31.346003),16);//设置地图中心位置和级别
map.setCurrentCity("上海");// 设置地图显示的城市 此项是必须设置的
map.addControl(new BMap.MapTypeControl());//添加地图类型控件，例如：卫星，三维
map.addControl(new BMap.NavigationControl());//左上角缩放控件

map.enableScrollWheelZoom();//启用滚轮放大缩小，默认禁用
map.enableContinuousZoom();//启用地图惯性拖拽，默认禁用
//=======================================


//自定义标注点
var point = new BMap.Point(121.502939,31.346003);
var marker = new BMap.Marker(point);  // 创建标注
map.addOverlay(marker);              // 将标注添加到地图中

//自定义创建信息窗口
var infoWindow = new BMap.InfoWindow("<p style='font-size:14px;color:#666;'><b>上海岸谷工业产品设计有限公司</b></</p><p style='font-size:14px'>地址：逸仙路3000号上海国际工业设计中心</p>");
map.openInfoWindow(infoWindow,point); //开启信息窗口

//============================================
$('#schroad').click(function(){
	var start=$('#startroad').val();
	var mothed=$('#mothedroad').val();
	if(mothed==1){
		if(start==''){alert('请输入出发地');}else{
			var transit = new BMap.TransitRoute("上海");
			transit.setSearchCompleteCallback(function(results){
				if (transit.getStatus() == BMAP_STATUS_SUCCESS){
					for (var index = 0; index < results.getNumPlans(); index++){
						var showElement = document.createElement("div");
						var callDLFunc = drawLine(map,results,index,showElement);
						showElement.style.lineHeight = "20px";
						showElement.style.margin = "0 0 5px";
						showElement.onclick=callDLFunc;
						showElement.innerHTML = (index + 1) + ". " + results.getPlan(index).getDescription();
						document.getElementById("r-result").appendChild(showElement);
						if(index == 0){showElement.onclick();}
					}
				}else{alert('没有搜索到相应路线');}
			});
			transit.search(start, "上海国际工业设计中心");
			var lastSetBackObj = null;
		}
	}else{
		if(start==''){alert('请输入出发地');}else{
		var options = {
			  onSearchComplete: function(results){
				if (driving.getStatus() == BMAP_STATUS_SUCCESS){
				  // 获取第一条方案
				  var plan = results.getPlan(0);
			
				  // 获取方案的驾车线路
				  var route = plan.getRoute(0);
			
				  // 获取每个关键步骤,并输出到页面
				  var s = [];
				  for (var i = 0; i < route.getNumSteps(); i ++){
					var step = route.getStep(i);
					s.push((i + 1) + ". " + step.getDescription());
				  }
				  document.getElementById("r-result").innerHTML = s.join("<br/>");
				}else{alert('没有搜索到相应路线');}
			  }
			};
		var driving = new BMap.DrivingRoute(map,options);
		driving.search(start, "上海国际工业设计中心");
		}
	}
	
});

function drawLine(aMap,results,index,obj){
    return function(){
        var opacity = 0.45;
        var planObj = results.getPlan(index);
        var bounds = new Array();
        var addMarkerFun = function(point,imgType,index,title){
            var url;
            var width;
            var height
            var myIcon;
            // imgType:1的场合，为起点和终点的图；2的场合为过程的图形
            if(imgType == 1){
                url = "http://map.baidu.com/image/dest_markers.png";
                width = 42;
                height = 34;
                myIcon = new BMap.Icon(url,new BMap.Size(width, height),{offset: new BMap.Size(14, 32),imageOffset: new BMap.Size(0, 0 - index * height)});
            }else{
                url = "http://map.baidu.com/image/trans_icons.png";
                width = 22;
                height = 25;
                var d = 25;
                var cha = 0;
                var jia = 0
                if(index == 2){
                    d = 21;
                    cha = 5;
                    jia = 1;
                }
                myIcon = new BMap.Icon(url,new BMap.Size(width, d),{offset: new BMap.Size(10, (11 + jia)),imageOffset: new BMap.Size(0, 0 - index * height - cha)});
            }

            var marker = new BMap.Marker(point, {icon: myIcon});
            if(title != null && title != ""){
                marker.setTitle(title);
            }
            // 起点和终点放在最上面
            if(imgType == 1){
                marker.setTop(true);
            }
            aMap.addOverlay(marker);
        }
        var addPoints = function(points){
            for(var i = 0; i < points.length; i++){
                bounds.push(points[i]);
            }
            
        }

        // 清空覆盖物
        aMap.clearOverlays();

        // 绘制驾车步行线路
        for (var i = 0; i < planObj.getNumRoutes(); i ++){
            var route = planObj.getRoute(i);
            if (route.getDistance(false) > 0){
                // 步行线路有可能为0
                aMap.addOverlay(new BMap.Polyline(route.getPath(), {strokeStyle:"dashed",strokeColor: "#30a208",strokeOpacity:0.75,strokeWeight:4,enableMassClear:true}));
            }
        }
        // 绘制公交线路
        for (i = 0; i < planObj.getNumLines(); i ++){
            var line = planObj.getLine(i);
            addPoints(line.getPath());
            // 公交
            if(line.type == BMAP_LINE_TYPE_BUS){
                // 上车
                addMarkerFun(line.getGetOnStop().point,2,2,line.getGetOnStop().title);
                // 下车
                addMarkerFun(line.getGetOffStop().point,2,2,line.getGetOffStop().title);
                
                // 地铁
            }else if(line.type == BMAP_LINE_TYPE_SUBWAY){
                // 上车
                addMarkerFun(line.getGetOnStop().point,2,3,line.getGetOnStop().title);
                // 下车
                addMarkerFun(line.getGetOffStop().point,2,3,line.getGetOffStop().title);
            }
            aMap.addOverlay(new BMap.Polyline(line.getPath(), {strokeColor: "#0030ff",strokeOpacity:opacity,strokeWeight:6,enableMassClear:true}));
        }

        aMap.setViewport(bounds);

        // 终点
        addMarkerFun(results.getEnd().point,1,1);
        // 开始点
        addMarkerFun(results.getStart().point,1,0);
        
        // 设置文字背景色
        if(lastSetBackObj != null){
            lastSetBackObj.style.backgroundColor = "#fff";
        }
        obj.style.backgroundColor = "#f0f0f0";
        lastSetBackObj = obj;
    }
}


});