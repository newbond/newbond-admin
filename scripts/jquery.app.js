$(function(){
//下拉菜单
	var tree_li=$('.nav_left > ul').find('li');
	checkChild(tree_li);
	tree_li.find('.in > a').click(function(){
		var $li=$(this).parent('.in').parent('li');
		$(this).parent().siblings('ul').is(':hidden')?show($li):hide($li);
	});
	tree_li.find('a:not(".chd")').click(function(){
		var pli=$(this).parent('.in').parent('li');
		pli.parents('#pleft').find('li').removeClass('current vipcurrent');
		pli.parents('#pleft').find('a').removeClass('curr');
		
		if($(this).siblings('span.vip').length < 1){
			pli.addClass('current');
		}else{
			pli.addClass('current vipcurrent');	
		}
		$(this).addClass('curr');
	});
	function checkChild(node){
		$.each(node,function(i,n){
			$(this).children('ul').hide();
			if($(this).children('ul').length > 0){
				$(this).children('.in').children('a').addClass('chd').after("<span class='tree_chd'></span>");
			}else{
				$(this).children('.in').append("<span class='tree_nochd'></span>");
			}
		});	
	}
	function show(node){
		node.children('ul').slideDown(300);
		node.children('.in').children('span.tree_chd').addClass('tree_open');
		node.siblings().find('ul').slideUp(300);
		node.siblings().find('span.tree_chd').removeClass('tree_open');
	}
	function hide(node){
		node.children('ul').slideUp(300);
		node.children('.in').children('span.tree_chd').removeClass('tree_open');
	}
//select效果
	var $select=$('.select');
	$.each($select,function(i,n){
		$(n).append('<input type="hidden" name="'+$(n).attr('name')+'" value="" />');
		$(n).children('a.curr').bind('click',function(){//下拉效果
			$(this).next('.show').is(':hidden')?selectShow($(this)):selectHide($(this));
		});
		$(n).find('ul.fm > li:not(".btn")').bind('click',function(event){//解除事件冒泡
			event.stopPropagation();
		});
		$(n).find('a.confirm').click(function(){//带表单的select列表
			var $checked=$(this).parents('ul').find('input:checked'),val='',text='',fg=',';
			$.each($checked,function(ii,nn){
				if(ii==$checked.length-1) fg='';
				val=val+$(nn).val()+fg;
				text=text+$(nn).parent('label').text()+fg;
			});
			$(this).parents('.show').prev('a.curr').html(text).siblings('input').val(val);
		});
		$(n).find('ul:not(".fm") > li').click(function(){//普通的select列表
			var val=$(this).attr('val'),text=$(this).text();
			$(this).parents('.show').prev('a.curr').html(text).siblings('input').val(val);
		});
	});
	function selectShow(node){
		node.next('.show').slideDown(200,function(){
			$('body').click(function(){
				selectHide(node);
			});	
		}).parent().css('z-index','100');
	}
	function selectHide(node){
		node.next('.show').slideUp(10,function(){
			$('body').unbind('click');	
		}).parent().css('z-index','1');	
	}
//----	
});